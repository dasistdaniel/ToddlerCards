import json

VERSION = '0.0.1'

def load_config(configfile):
    with open(configfile, 'r') as fileConfig:
        data=fileConfig.read()
    config = json.loads(data)
    return config

def list_modules(config):
    modules_list = []
    for module in config['modules']:
        modules_list.append(module)
    return modules_list

def list_cards(module):
    cards_list = []
    for card in config['modules'][module]['cards']:
        cards_list.append(card)
    return cards_list

def list_commands(module, card):
    command_list = []
    for command in config['modules'][module]['cards'][card]["commands"]:
        command_list.append(command)
    return command_list

config = load_config('config.json')
modules = list_modules(config)
cards = list_cards(modules[1])
commands = list_commands(modules[1], cards[0])

print ('Printing Config')
print (config)
print ()
print (modules)
print ()
print (cards)
print ()
print (commands)


print ()
print ()
print ()
print ()
print ()
print (f'Welcome to ToddlerCards {VERSION}')
print ()

while True:
    cardInput = input('Just swipe the Card: ')
    if cardInput in cards:
        print (f'Got this: {cardInput} - {list_commands(modules[0], cardInput)[0]["command"]}: {list_commands(modules[0], cardInput)[0]["value"]}')
    else:
        print ('Card not found.')

    
